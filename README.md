# TP3 : Object-oriented programming

## ABOUT :
this program creates a chained list, addes, deletes nodes.

## Installation :

To install this script use the following commands :

mkdir ~/Desktop/samerelkhoury/
cd ~/Desktop/samerelkhoury/
git clone https://gitlab.com/samerelkhoury/tp3.git

==> You're now good to go!

## COMMANDS TO RUN THE PROGRAM :
python3 chained_list.py

## Authors :
SAMER EL KHOURY

