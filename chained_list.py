"""
    Python Script to creat a chained list, add and delete nodes
"""

__author__ = "Samer El Khoury"


class Node:
    """
    Creates nodes for the chained list
    """
    def __init__(self, value):
        self.value = value
        self.next = None
    def __str__(self):
        #return str(self.value)
        string = str(self.value)
        if self.next is not None:
            string += " -> " + str(self.next)
        return string

    def get_node_value(self):
        """
        returns node value
        """
        return self.value

class ChainedList:
    """
    Defines the chained list
    """
    def __init__(self):
        self.first_node = None

    def add_node(self, value):
        """
        method to add a node to the chained list
        """
        if self.first_node is None:
            self.first_node = Node(value)
        elif value < self.first_node.value:
            node = Node(value)
            node.next = self.first_node
            self.first_node = node
        else:
            node = Node(value)
            next_node = self.first_node.next
            while next_node is not None and node.value > next_node.value:
                previous_node = next_node
                next_node = next_node.next
            node.next = next_node
            previous_node.next = node
            if next_node is None:
                previous_node.next = node
            else:
                node.next = next_node
                previous_node.next = node

    def delete_node(self, value):
        """
        method to remove a node if it exists
        """
        if self.first_node is None:
            return "ERROR: THE LIST IS EMPTY !"
        if value < self.first_node.value:
            return "THE NODE DOESN'T EXIST"
        current_node = self.first_node
        previous_node = self.first_node
        if current_node.next is None and current_node.value == value:
            self.first_node = None
            return
        while current_node is not None and value != current_node.value:
            previous_node = current_node
            current_node = current_node.next
        if current_node is None:
            return "THE NODE DOESN'T EXIST"
        else:
            previous_node.next = current_node.next

    def __str__(self):
        if self.first_node is None:
            return "liste vide"
        return str(self.first_node)

L = ChainedList()
print(L)
L.add_node(1)
L.add_node(0)
print(L)
L.add_node(-1)
L.add_node(2)
print(L)
L.add_node(5)
print(L)
L.delete_node(2)
print(L)
print(L.delete_node(-4))
